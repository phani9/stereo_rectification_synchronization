import numpy as np
import cv2
import glob
import argparse
import sys

# Set the values for your cameras
W = 1920
H = 1080 

def gstreamer_pipeline(
    sensor_id=0,
    sensor_mode=3,
    capture_width=W,
    capture_height=H,
    display_width=W,
    display_height=H,
    framerate=30,
    flip_method=0,
):
    return (
        "nvarguscamerasrc sensor-id=%d sensor-mode=%d ! "
        "video/x-raw(memory:NVMM), "
        "width=(int)%d, height=(int)%d, "
        "format=(string)NV12, framerate=(fraction)%d/1 ! "
        "nvvidconv flip-method=%d ! "
        "video/x-raw, width=(int)%d, height=(int)%d, format=(string)BGRx ! "
        "videoconvert ! "
        "video/x-raw, format=(string)BGR ! appsink"
        % (
            sensor_id,
            sensor_mode,
            capture_width,
            capture_height,
            framerate,
            flip_method,
            display_width,
            display_height,
        )
    )



#gstreamer_pipeline_string_0 = gstreamer_pipeline(sensor_id=0)
#gstreamer_pipeline_string_1 = gstreamer_pipeline(sensor_id=1)
pipeline_0 = "nvarguscamerasrc sensor-id=0 sensor-mode=3 tnr-mode=0 ee-mode=0 exposuretimerange='8000000 8000000'! video/x-raw(memory:NVMM), width=(int)1920, height=(int)1080, format=(string)NV12, framerate=(fraction)21/1 ! nvvidconv flip-method=0 ! video/x-raw, width=(int)1920, height=(int)1080, format=(string)BGRx ! videoconvert ! video/x-raw, format=(string)BGR ! appsink"
#pipeline_0 = "nvarguscamerasrc sensor-id=0 sensor-mode=3 ! video/x-raw(memory:NVMM), width=(int)3820, height=(int)2464, format=(string)NV12, framerate=(fraction)21/1 ! nvvidconv flip-method=0 ! video/x-raw, width=(int)1920, height=(int)1080, format=(string)BGRx ! videoconvert ! video/x-raw, format=(string)BGR ! appsink"
pipeline_1 = "nvarguscamerasrc sensor-id=1 sensor-mode=3 tnr-mode=0 ee-mode=0 exposuretimerange='8000000 8000000'! video/x-raw(memory:NVMM), width=(int)1920, height=(int)1080, format=(string)NV12, framerate=(fraction)21/1 ! nvvidconv flip-method=0 ! video/x-raw, width=(int)1920, height=(int)1080, format=(string)BGRx ! videoconvert ! video/x-raw, format=(string)BGR ! appsink"

capL = cv2.VideoCapture(pipeline_0, cv2.CAP_GSTREAMER)
capR = cv2.VideoCapture(pipeline_1, cv2.CAP_GSTREAMER)


i = 0


def main():
    global i

    i = 1000  # Get the start number.
    base_dir = 'images/'

    count = 1001
    while True:
        # Grab and retreive for sync
        if not (capL.grab() and capR.grab()):
            print("No more frames")
            break

        _, leftFrame = capL.retrieve()
        _, rightFrame = capR.retrieve()

        # Use if you need high resolution. If you set the camera for high res, you can pass these.
        # cv2.namedWindow('capL', cv2.WINDOW_NORMAL)
        # cv2.resizeWindow('capL', 1024, 768)

        # cv2.namedWindow('capR', cv2.WINDOW_NORMAL)
        # cv2.resizeWindow('capR', 1024, 768)

        #cv2.imshow('capL', leftFrame)
        #cv2.imshow('capR', rightFrame)
        out = np.hstack((leftFrame, rightFrame))
        out = cv2.resize(out, (0, 0), fx=.5, fy=.5)
        cv2.imshow('out', out)
        count+=1
        if count%10==0:
            print ('saving ', i+1, end='\r')
            #cv2.imwrite(base_dir + "/left" + str(i) + ".png", out)
            cv2.imwrite(base_dir + "/left" + str(i) + ".png", leftFrame)
            cv2.imwrite(base_dir + "/right" + str(i) + ".png", rightFrame)
            i += 1
            if count>5000:
                count=1001

        key = cv2.waitKey(5)
        if key == ord('q'):
            break
        elif key == ord('c'):
            print ('saving ', i+1, end='\r')
            cv2.imwrite(base_dir + "/left" + str(i) + ".png", leftFrame)
            cv2.imwrite(base_dir + "/right" + str(i) + ".png", rightFrame)
            i += 1

    cv2.destroyAllWindows()
    capL.release()
    capR.release()


if __name__ == '__main__':
    main()
