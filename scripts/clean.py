import glob
import cv2
import numpy as np
import os


lnames = glob.glob('../stereo_set_new_2/left/*')
lnames.sort()
rnames = glob.glob('../stereo_set_new_2/right/*')
rnames.sort()



for i in range(len(lnames)):
    name = 'img-' + str(i+1000)+ '.png'
    oname = '../stereo_set_new_2/left/' + name
    os.system('mv ' + lnames[i]  + ' ' + oname)
    oname = '../stereo_set_new_2/right/' + name
    os.system('mv ' + rnames[i]  + ' ' + oname)


