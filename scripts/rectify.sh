python3 single_camera_calibration.py --image_dir ../stereo_set_new_2/right/ --image_format png --prefix img --square_size 0.025 --width 9 --height 6 --save_file right_cam.yml
python3 single_camera_calibration.py --image_dir ../stereo_set_new_2/left/ --image_format png --prefix img --square_size 0.025 --width 9 --height 6 --save_file left_cam.yml

python stereo_camera_calibration.py --left_file left_cam.yml --right_file right_cam.yml --left_prefix img --right_prefix img --left_dir ../stereo_set_new_2/left/ --right_dir ../stereo_set_new_2/right/ --image_format png --square_size 0.025 --save_file stereo_cam.yml
