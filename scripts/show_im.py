import cv2
import glob
import numpy as np

fnames = glob.glob('images/left*')
for lname in fnames:
    left = cv2.imread(lname)
    rname = lname.replace('left','right')
    right = cv2.imread(rname)
    out = np.hstack((left, right))
    out = cv2.resize(out, (0, 0), fx=.4, fy=.4)
    cv2.imshow('c',out)
    cv2.waitKey(0)


