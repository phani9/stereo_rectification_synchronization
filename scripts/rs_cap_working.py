import time
import pyrealsense2 as rs
import uuid

import cv2
from uuid import uuid1
import numpy as np
import os

def gstreamer_pipeline(
    sensor_id=0,
    capture_width=1280,
    capture_height=720,
    framerate=60,
    flip_method=0,
):
    return (
        "nvarguscamerasrc sensor-id=%d ! "
        "video/x-raw(memory:NVMM), "
        "width=(int)%d, height=(int)%d, "
        "format=(string)NV12, framerate=(fraction)%d/1 ! "
        "nvvidconv flip-method=%d ! "
        "video/x-raw, format=(string)BGRx ! "
        "videoconvert ! "
        "video/x-raw, format=(string)BGR ! appsink max-buffers=1 drop=True"
        % (
            sensor_id,
            capture_width,
            capture_height,
            framerate,
            flip_method,
        )
    )


gstreamer_pipeline_string =  gstreamer_pipeline(
            sensor_id=0,
            flip_method=0,
        )
left = cv2.VideoCapture(gstreamer_pipeline_string, cv2.CAP_GSTREAMER)
#left.set(cv2.CAP_PROP_BUFFERSIZE, 1)


pipeline = rs.pipeline()
config = rs.config()
config.enable_stream(rs.stream.color, 848, 480, rs.format.bgr8, 60)
profile = pipeline.start(config)

rs_prev = 0
prev = time.time()
pt = prev

print ('Space to save; Esc to quit')

rgb_dir = '/tmp/data/rgb/'
os.system('mkdir -p ' + rgb_dir)
counter = 50

while True:
                frames = pipeline.wait_for_frames()
                if not left.grab():
                    continue
                #ts = left.get( cv2.CAP_PROP_POS_MSEC)
                tm = 1000*(time.time()-prev)
                _, left_frame = left.retrieve()

                # Align the depth frame to color frame
                color_frame = frames.get_color_frame()
                color_image = np.asanyarray(color_frame.get_data())
                #color_image = cv2.resize(color_image, (640, 360))
                #left_frame = cv2.resize(left_frame, (640, 360))
                #out = np.hstack((color_image, left_frame))


                #rs_ts = rs.frame.get_frame_metadata(frames, rs.frame_metadata_value.backend_timestamp)#.time_of_arrival
                rs_ts = rs.frame.get_frame_metadata(frames, rs.frame_metadata_value.time_of_arrival)
                #rs_ts = frames.get_timestamp()
                rs_ts = int(rs_ts)
                imx_ts = int(1000*time.time())
                oname = '/tmp/images/left/' + str(rs_ts) + '.png'
                cv2.imwrite(oname, color_image)
                oname = '/tmp/images/right/' + str(imx_ts) + '.png'
                cv2.imwrite(oname, left_frame)
                #time.sleep(2)
                color_image = cv2.resize(color_image, (640, 360))
                left_frame = cv2.resize(left_frame, (640, 360))
                out = np.hstack((color_image, left_frame))
                cv2.imshow('c',out)
                k = cv2.waitKey(0)
                if k==27:    # Esc key to stop
                    break


                counter-=1
                if not counter:
                    break

                continue

                cv2.imshow('c',out)
                cv2.waitKey(20)



                name = str(uuid.uuid1())+ '.png'

                oname = depth_dir + name
                cv2.imwrite(oname,depth_image)

                oname = depth_dir_aligned + name
                cv2.imwrite(oname,  depth_image_aligned)

                oname = rgb_dir + name
                cv2.imwrite(oname,  color_image)
                print ('saving '  + oname)
                time.sleep(.1)
pipeline.stop()
left.release()
